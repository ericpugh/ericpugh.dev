module.exports = {
  theme: {
    extend: {},
    colors: {
      transparent: 'transparent',
      black: '#101820',
      'off-black': '#272822',
      white: '#fff',
      'off-white': '#f8f8f2',
      red: {
        light: "#ff67a0",
        base: "#F92672",
        dark: "#c00047",
      },
      blue: {
        light: "#9dffff",
        base: "#66d9ef",
        dark: "#23a7bc",
      },
      green: {
        light: "#dbff66",
        base: "#a6e22e",
        dark: "#72b000",
      },
      yellow: {
        light: "#ffffa5",
        base: "#e6db74",
        dark: "#b2aa45",
      },
      orange: {
        light: "#ffc855",
        base: "#fd971f",
        dark: "#c46800",
      },
      purple: {
        light: "#e2b1ff",
        base: "#ae81ff",
        dark: "#7b53cb",
      },
      gray: {
        light: "#dfdfe2",
        base: "#a1a1a6",
        dark: "#5f5f68",
      }
    },

    fontFamily: {
      heading: ['Raleway', 'Open Sans', 'sans-serif'],
      hand: ['Gochi Hand', 'cursive'],
      sans: [
        'Open Sans',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        '"Helvetica Neue"',
        'Arial',
        '"Noto Sans"',
        'sans-serif',
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"',
      ],
      serif: ['Georgia', 'Cambria', '"Times New Roman"', 'Times', 'serif'],
      mono: ['Menlo', 'Monaco', 'Consolas', '"Liberation Mono"', '"Courier New"', 'monospace'],
    },

  },
  variants: {},
  plugins: []
}
