# ericpugh.dev

Personal site, [ericpugh.dev](https://ericpugh.dev). 

Built with [Hugo](https://gohugo.io/) and using [TailwindCSS](https://tailwindcss.com/).

## Theme
The theme "devloper" was adapted from [hugo-theme-tailwindcss-starter](https://github.com/dirkolbrich/hugo-theme-tailwindcss-starter).
As explained in that repository you'll want to install `postcss-cli` and `autoprefixer` globally in your environment, as [Hugo Pipe’s PostCSS requires it](https://gohugo.io/hugo-pipes/postcss/).

## Doing Stuff

Start Hugo locally
```shell script
hugo server --disableFastRender --buildDrafts
```

Build the static site for development/authoring
```shell script
hugo -DF
```
