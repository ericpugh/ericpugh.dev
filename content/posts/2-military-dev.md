---
id: 2
draft: true
title: "The original \"bootcamp\""
subtitle: "How being in the military made me a better web developer."
date: 2020-03-06
cover_image: ""
color: green
description: "Years ago I was in the Army. Strangely, over the years I've noticed a lot of overlap between methodologies used in the armed forces and those preached by our programming gurus. It turns out being a soldier might have made me a better web developer. (maybe)"
tags: ["meta", "career"]
keywords: ["web development", "training", "productivity", "career"]
---

Years ago I was in the Army, the _Bill Clinton_ Army. I was a "Network Switching Operator", which although technically is something technical,
didn't help me become a web developer in any obvious way. However, I've noticed there's a lot of overlap between the military 
and web development. For example, collaboration, effective communication, adaptability, motivation, are all things you might 
read about in a blog post about effective software development, but they're also things you could have read about in an [Army field manual](https://usacac.army.mil/sites/default/files/misc/doctrine/CDG/fms.html) 
decades before the [Agile Manifesto](http://agilemanifesto.org/) was written. (Man, the Army loves them some documentation). 
I guess it makes sense that the Army would be focused on leadership and training methods, when you consider the primary function of the 
military is really only one of two things at a time:

 1. Go to war.
 2. Train for going to war.
 
Since, as a software developers we're "knowledge workers", our field is always going to revolve around training and
gaining new knowledge and expertise. (With the added benefit of not having to go to war). But I think it's interesting 
that I got a lot of the same advice about teamwork and productivity from an obnoxious, tobacco dipping Staff Sergeant as 
I have from IT industry "thought leaders" and "influencers".

## Army Training, Sir!

Here's my summary of Army training and how it's related to web development. First you have your Basic Training, which is mostly physical activity and various physcological stress tests.
(It's basically the same as assigning a dev pointless Internet Explorer bugs to troubleshoot for days on end, just to see if they'll snap). Then, depending
on your [MOS](https://en.wikipedia.org/wiki/United_States_military_occupation_code) (your job) you have a much longer training 
in a classroom or trade school environment to learn the basics of your job. Finally, for the rest of your time in the military 
you'll have a sort of on-the-job training called "Sergeant's Time". (I guess they still do this, it's been a **long** time). This is a training day you
take once a month, where you work on improving and refreshing your job skills. It's informal, usually away from your regular
 place of work, and often lead by junior team members. It's also a day to experiment. What would happen if we had to connect
 our node center to one of those Macedonian Army teams whose connectors are just frayed copper wires? I don't know. Let's 
 start splicing cables and find out! The point is, it's about always leveling up, experimenting, and spreading that knowledge 
 across the team. One of the worst kinds of software developers you're going to work with is the one who has one _thing_ they do. 
 They have knowledge about that one specific _thing_, and maybe it's important, but it's that's all they do. Need something else done? Nope, that's not 
 their _thing_. They also don't usually see any reason to share their knowledge with their colleagues. (They might steal my precious _thing!_) 
 These types aren't good soldiers (or developers). You're not going to be successful as a web developer (where things 
 change at a break neck pace) without and constantly trying to better yourself, if not reinvent yourself. And your going have a 
 a higher chance of success if you are open and transparent and do those things as part of a team. Likewise, you're not going 
 to be a good team lead without creating an environment where the people on your team can do that.
  
> "One team, one fight!"
> &mdash; Army Guy
>
Something else the Army does a lot is Cross-Training. Other than purposely moving people around to work on and learn different parts of your stated job, you're going have random training that has nothing to do with your job, 
 like land navigation, general engineering, or emergency medical training. (Hi. I'm a PHP developer, I'm going to give you an IV, hopefully I can find a vein :no_mouth:).
The point is you should try to have a broad base of knowledge. 

The Army pays a lot of attention to routines, team building, and morale. (To the point of being pathological.) First routines,
just one example is the morning formation in the "motor pool". This is a short status meeting with the entire Unit,
that when adjourned becomes an update with the smaller department (Platoon), which finally becomes a daily update
from the team. This happens everyday, at a specific time, to start and close the days' business. Basically, the Army invented and 
perfected the daily standup. Also, there's countless examples of how the Army attempts to build effective teams. One common team
 building exercise is to pretend that certain people have been eliminated during a training exercise, which requires the remaining 
 team members to perform their duties. It could be anything from the DevOps guy has been "taken out", so now the developer 
 has to deploy her own code (DevOps guy *scoffs*) to the intern whose now running the entire company. This is actually pretty 
 effective, and reminds me of the idea of "self managed" teams. But probably the most obvious team building activity is the "battle buddy". 
 Throughout your time in the military you'll be assigned a battle buddy. Basically, another person you're responsible for and
  vice versa. If your battle body screws up, you'll both have to clean up the mess. If your battle buddy is performing poorly
  it's your responsibility for not guiding them. If this sounds silly, it _is_, but it does have a couple interesting consequences.
  For one, it forces pretty intense cooperation among team members. If everybody on a team has a battle buddy, then the success
  or failings of any individual team member effects everyone. This also tends to encourage accountability among team members.
  (Nobody's wasting all day writing pointless Confluence pages under the battle buddy system!) Basic human psychology can be 
  pretty effective, go figure. Finally, the gas chamber. Every once in a while we'd fill a tent with tear gas, and groups
  would go in with gas masks, and then take them off to see how long they could stand it, until eventually everyone would
  fall out of the tent, blinded, like clowns from a clown car. It was bizarre. Okay, I'm not exactly sure how this one relates
  to web development, but it is an example of a team experience that's hard to replicate in a Slack chat.   

None of this is to say the Army is without it's faults. It's not exactly an ideal environment 
to foster critical thinking skills. (I mean, your clothes are a uniform!) It has it's own kind of creepy "corporate culture", 
made worse by that cultures' pervasiveness in your life. (Seriously, they have their own [justice system](https://en.wikipedia.org/wiki/Uniform_Code_of_Military_Justice))
And work-life balance is... not balanced. I could go on. That being said, the Army _did_ make me a better web developer.
I should have probably just went to "Javascript bootcamp", but I ended up at the original (Basic Combat Training) bootcamp instead. 
What I learned is, regardless of what you end up doing, you should listen to that obnoxious Staff Sergeant. (He's seen some shit.)

