---
title: About
description: "About the author, Eric Pugh"
tags: [meta]
---

Hi :wave: 

I'm a developer who's been fiddling around with different web technologies since the olden days.
I'm primarily a PHP developer, work with [Drupal](https://drupal.org/ericpugh) quite a bit, and do a lot of frontend type things. 
Generally, just trying to be a good generalist. In part, touching many different parts of the "stack" has been a necessity 
as I've worked in a lot of different fields, with different resources, needs, and understanding of the web, including 
[museums](https://americanart.si.edu), [government](https://www.army.mil/), [financial sector](https://www.financialresearch.gov/), and [print](https://stripes.com)/[broadcast](https://www.usagm.gov/) journalism. 
Mostly though, this work has just given me a chance to do what I love to do... fiddle around. (It's really not a bad gig.)

I have a stack of things I want to read, listen to, and learn. I've decided to start this blog (after several failed blog 
attempts over the years) to try to turn some of those things into something concrete. Like any good side 
project (destined to be abandoned) you have to start out with some lofty goals. So, my goals for this site are the following:

0. Learn a something new. (_+2_ points for new framework/language, _+3_ if it's [Go](https://golang.org/), _+1_ point for all other knowledge gained).
1. Share something useful. (_+1_ point. _+2_ points if that something that can be installed with a dependency manager).
2. Help someone with a problem they're having (_+1_ point for technical problem, _+11_ points for existential or other psychological crisis)
3. Come up with a clever activity for teaching a toddler to code. (Subtract the child's age from 7, a positive integer result is the awarded point total.)
4. Draw some silly pictures. (No points award)

Once each of the five goals have been accomplished (or a combined score of >=11 points reached) this site will be deleted 
forever. Afterward, the $9.99/year saved on a vanity domain name will be donated to the [Internet Archive](https://archive.org/donate/) 
which will archive a copy (**BOOM**, free hosting). That's the _whole_ plan.

~Eric
